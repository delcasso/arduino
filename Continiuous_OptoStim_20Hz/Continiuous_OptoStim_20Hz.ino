int laser_pin=11;
void setup() {
  pinMode(laser_pin, OUTPUT);
}
void loop() {
    digitalWrite(laser_pin, HIGH);
    delay(5);      
    digitalWrite(laser_pin, LOW);
    delay(45);
}
