void setup() {
  // put your setup code here, to run once:

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(laser_pin, OUTPUT);
  pinMode(red_led_pin, OUTPUT);
  digitalWrite(red_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(red_led_pin, LOW);   // turn the LED on (HIGH is the voltage level)  
 
  // put your main code here, to run repeatedly:

}  delay(off_period_ms);
  
  //5 min. Laser ON, 20Hz (5ms ON, 45ms OFF)
  digitalWrite(red_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  for (int i = 1; i <= 6000; i++) {
    digitalWrite(laser_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(5);                       // wait for a second
    digitalWrite(laser_pin, LOW);    // turn the LED off by making the voltage LOW
    delay(45);
  }  
  digitalWrite(red_led_pin, LOW);   // turn the LED on (HIGH is the voltage level)

  delay(off_period_ms);
  
  delay(end_period_ms);
