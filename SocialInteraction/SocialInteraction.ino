
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

unsigned long off_period_ms = 300000;
unsigned long end_period_ms = 3600000;
int laser_pin=11;
int red_led_pin=12;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(laser_pin, OUTPUT);
  pinMode(red_led_pin, OUTPUT);
  digitalWrite(red_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(red_led_pin, LOW);   // turn the LED on (HIGH is the voltage level)  
  
}

// the loop function runs over and over again forever
void loop() {

  //___________________________________________________________________________________
  //BLOCK 1, 5 min. Laser OFF  
  delay(off_period_ms);

  //___________________________________________________________________________________
  //BLOCK 2, 5 min. Laser ON, 20Hz (5ms ON, 45ms OFF)
  // 3min a 20 Hz, 180 sec a 20 Hz, 300*20 evts, 6000 evts
  digitalWrite(red_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  for (int i = 1; i <= 6000; i++) {
    digitalWrite(laser_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(5);                       // wait for a second
    digitalWrite(laser_pin, LOW);    // turn the LED off by making the voltage LOW
    delay(45);
  }
  digitalWrite(red_led_pin, LOW);   // turn the LED on (HIGH is the voltage level)

  //___________________________________________________________________________________
  //BLOCK 3, 5 min. Laser OFF
  delay(off_period_ms);

  //___________________________________________________________________________________
  //BLOCK 4, 5 min. Laser ON, 20Hz (5ms ON, 45ms OFF)
  // 3min a 20 Hz, 300 sec a 20 Hz, 300*20 evts, 6000 evts
  digitalWrite(red_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  for (int i = 1; i <= 6000; i++) {
    digitalWrite(laser_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(5);                       // wait for a second
    digitalWrite(laser_pin, LOW);    // turn the LED off by making the voltage LOW
    delay(45);
  }
  digitalWrite(red_led_pin, LOW);   // turn the LED on (HIGH is the voltage level)

  delay(end_period_ms);
  
}
