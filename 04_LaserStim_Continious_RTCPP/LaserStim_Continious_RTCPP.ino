
unsigned long off_period_ms = 300000;
unsigned long end_period_ms = 3600000;
int laserPin = 11;
int redLedPin = 12;
int bonsaiOptoStimPin = 7;
bool bonsaiOptoStimRequest = false;
//bool startSignal = false;

bool stimAlreadyOn = false;



// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(laserPin, OUTPUT);
  pinMode(redLedPin, OUTPUT);
  pinMode(bonsaiOptoStimPin, INPUT);
  digitalWrite(redLedPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(redLedPin, LOW);   // turn the LED on (HIGH is the voltage level)
}

// the loop function runs over and over again forever
void loop() {

  bonsaiOptoStimRequest = digitalRead(bonsaiOptoStimPin);

  if (bonsaiOptoStimRequest)
  {
    if (stimAlreadyOn == false)
    {
      digitalWrite(redLedPin, HIGH);
      digitalWrite(laserPin, HIGH);
      stimAlreadyOn = true;
    }
  }
  else
  {
    if (stimAlreadyOn == true)
    {
      digitalWrite(redLedPin, LOW);
      digitalWrite(laserPin, LOW);
      stimAlreadyOn = false;
    }
  }
}


