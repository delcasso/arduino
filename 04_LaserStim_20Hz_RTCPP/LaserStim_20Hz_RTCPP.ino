/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

unsigned long off_period_ms = 300000;
unsigned long end_period_ms = 3600000;
int laserPin=11;
int redLedPin=12;
int bonsaiOptoStimPin=7;
bool bonsaiOptoStimRequest = false;
//bool startSignal = false;


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(laserPin, OUTPUT);
  pinMode(redLedPin, OUTPUT);
  pinMode(bonsaiOptoStimPin, INPUT);
  digitalWrite(redLedPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(redLedPin, LOW);   // turn the LED on (HIGH is the voltage level)  
}

// the loop function runs over and over again forever
void loop() { 
  
    bonsaiOptoStimRequest=digitalRead(bonsaiOptoStimPin);
    
    if(bonsaiOptoStimRequest)
    {
    // 3min a 20 Hz, 300 sec a 20 Hz, 300*20 evts, 6000 evts
    digitalWrite(redLedPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    digitalWrite(laserPin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(5);                       // wait for a second
    digitalWrite(laserPin, LOW);    // turn the LED off by making the voltage LOW
    delay(45);    
    digitalWrite(redLedPin, LOW);   // turn the LED on (HIGH is the voltage level) 
    }
    
}
