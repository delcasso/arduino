unsigned long off_period_ms = 300000;
unsigned long end_period_ms = 3600000;
int laser_pin=11;
int red_led_pin=12;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(laser_pin, OUTPUT);
  pinMode(red_led_pin, OUTPUT);
  digitalWrite(red_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(red_led_pin, LOW);   // turn the LED on (HIGH is the voltage level)  
  
}

// the loop function runs over and over again forever
void loop() {
  //5 min. Laser ON, 20Hz (5ms ON, 45ms OFF)
  // 3min a 20 Hz, 180 sec a 20 Hz, 300*20 evts, 6000 evts
  digitalWrite(red_led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  for (int i = 1; i <= 6000; i++) {
    digitalWrite(laser_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(5);                       // wait for a second
    digitalWrite(laser_pin, LOW);    // turn the LED off by making the voltage LOW
    delay(45);
  }
  digitalWrite(red_led_pin, LOW);   // turn the LED on (HIGH is the voltage level)

  //___________________________________________________________________________________
  //BLOCK 3, 5 min. Laser OFF
  delay(end_period_ms);
  
}
