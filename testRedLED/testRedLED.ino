
int red_led_pin = 12;
int laser_pin=11;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(laser_pin, OUTPUT);
  pinMode(red_led_pin, OUTPUT);
  digitalWrite(laser_pin, LOW); 
  digitalWrite(red_led_pin, HIGH);

}

// the loop function runs over and over again forever
void loop() {

delay(1000);

}
