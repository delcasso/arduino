

// These constants won't change. They're used to give names to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int ledPin = 9; // Analog output pin that the LED is attached to
int nLicks =0;
bool animalIsLicking = false;

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
}

void loop() {
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  Serial.print(sensorValue);

  if (sensorValue==1023)
  {      
    if (animalIsLicking==false)
    {
      digitalWrite(ledPin, LOW);
      animalIsLicking = true;
      nLicks++;
      Serial.print(millis());
      Serial.print("\t");
      Serial.println(nLicks);
    }
       
  }
       
  else
  {  
    if (animalIsLicking==true)
    {
      digitalWrite(ledPin, HIGH);
     animalIsLicking = false;
      Serial.print(millis());
      Serial.println("\t0");
    }   
  }
  

  // print the results to the Serial Monitor:


  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
 // delay(2);
}
