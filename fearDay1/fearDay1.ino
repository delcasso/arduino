
int redLedPin = 12;
int laserPin = 11;
int speakerPin = 3;
int shockerPin = 2;
int boxLedPin = 4;

int toneFreq=8000;

int soundDuration_ms=5000;//5000
int shockDuration_ms=1000;//1000

unsigned long min_ITI_ms = 5000;//50000
unsigned long max_ITI_ms = 70000;//70000 

unsigned long end_period_ms = 3600000; //3600000;

int nTrials = 16;

void setup() {
  // put your setup code here, to run once:
  pinMode(redLedPin, OUTPUT);
  pinMode(laserPin, OUTPUT);
  pinMode(speakerPin, OUTPUT);
  pinMode(shockerPin, OUTPUT);
  pinMode(boxLedPin, OUTPUT);
  
  digitalWrite(redLedPin, LOW);
  digitalWrite(laserPin, LOW);
  digitalWrite(speakerPin, LOW);
  digitalWrite(shockerPin, HIGH);
  digitalWrite(boxLedPin, LOW);
  digitalWrite(redLedPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(redLedPin, LOW);   // turn the LED off (LOW is the voltage level) 
  
}


void loop() {
  delay(60000);
  for (int i = 1; i <= nTrials; i++) {
        digitalWrite(boxLedPin, HIGH);
        tone(speakerPin,toneFreq);
        delay(soundDuration_ms-shockDuration_ms);
        digitalWrite(shockerPin, LOW);
        delay(shockDuration_ms);
        digitalWrite(shockerPin, HIGH);
        noTone(speakerPin);
        digitalWrite(boxLedPin, LOW);
        delay(random(min_ITI_ms,max_ITI_ms));      
  }
  
  delay(end_period_ms);
  
  
}
