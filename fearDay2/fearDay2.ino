
int laserLedPin = 12;
int laserPin = 11;
int speakerPin = 3;
int shockerPin = 2;
int speakerLedPin = 4;

int toneFreq = 8000;

int soundDuration_ms = 5000; //5000
int shockDuration_ms = 1000; //1000
int trialTypethreeDuration_ms = 5000;//5000

unsigned long min_ITI_ms = 50000;//50000
unsigned long max_ITI_ms = 70000;//70000

unsigned long end_period_ms = 3600000;

int nTrials = 24;

int trialType[24] = {2, 3, 1, 2, 1, 3, 3, 2, 1, 3, 1, 2, 3, 2, 1, 2, 1, 3, 3, 2, 1, 3, 1, 2};
// type 1 = CS-US ON   // type 2 = CS-US OFF    // type 3 = ON

int LaserTrain = 100;

void setup() {
  // put your setup code here, to run once:
  pinMode(laserLedPin, OUTPUT);
  pinMode(laserPin, OUTPUT);
  pinMode(speakerPin, OUTPUT);
  pinMode(shockerPin, OUTPUT);
  pinMode(speakerLedPin, OUTPUT);

  digitalWrite(laserLedPin, LOW);
  digitalWrite(laserPin, LOW);
  digitalWrite(speakerPin, LOW);
  digitalWrite(shockerPin, HIGH);
  digitalWrite(speakerLedPin, LOW);

  digitalWrite(laserLedPin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);
  digitalWrite(laserLedPin, LOW);   // turn the LED off (LOW is the voltage level)

}

void loop() {
  
  delay(60000);
  
  for (int i = 0; i <= nTrials; i++) {

    switch (trialType[i]) {
      case 1:
        digitalWrite(laserLedPin, HIGH);   // turn the LED on (HIGH is the voltage level)
        digitalWrite(speakerLedPin, HIGH);
        tone(speakerPin, toneFreq);        
        for (int j = 1; j <= 80; j++)
        { digitalWrite(laserPin, HIGH); delay(5);  // turn the Laser on (HIGH is the voltage level)
          digitalWrite(laserPin, LOW); delay(45);
        }               
        digitalWrite(shockerPin, LOW);
        for (int k = 1; k <= 20; k++)
        { digitalWrite(laserPin, HIGH); delay(5);  // turn the Laser on (HIGH is the voltage level)
          digitalWrite(laserPin, LOW); delay(45);
        }   // turn the Laser off by making the voltage LOW      
        digitalWrite(shockerPin, HIGH);
        noTone(speakerPin);
        digitalWrite(speakerLedPin, LOW);     
        digitalWrite(laserLedPin, LOW);   // turn the LED off (LOW is the voltage level)
        break;
        
      case 2:
        digitalWrite(speakerLedPin, HIGH);
        tone(speakerPin, toneFreq);
        delay(soundDuration_ms - shockDuration_ms);
        digitalWrite(shockerPin, LOW);
        delay(shockDuration_ms);
        digitalWrite(shockerPin, HIGH);
        noTone(speakerPin);
        digitalWrite(speakerLedPin, LOW);
        break;
        
      case 3:
        digitalWrite(laserLedPin, HIGH);   // turn the LED on (HIGH is the voltage level)
        for (int m = 1; m <= 100; m++)
        { digitalWrite(laserPin, HIGH); delay(5);  // turn the Laser on (HIGH is the voltage level)
          digitalWrite(laserPin, LOW); delay(45);
        }   // turn the Laser off by making the voltage LOW
        digitalWrite(laserLedPin, LOW);   // turn the LED off (LOW is the voltage level)
        break;
        
      default:
        // if nothing else matches, do the default
        // default is optional
        break;
    }
    delay(random(min_ITI_ms, max_ITI_ms));
  }
  delay(end_period_ms);
}
