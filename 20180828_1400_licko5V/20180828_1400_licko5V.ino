
const int lickThresholdInf = 20;
const int lickThresholdSup = 50;

const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int ledPin = 9; // Analog output pin that the LED is attached to

int nLicks = 0;

bool animalIsLicking = false;

int sensorValue = 0;        // value read from the pot
int outputValue = lickThresholdInf;        // value output to the PWM (analog out)



void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
}

void loop() {
  // read the analog in value:
  sensorValue = analogRead(analogInPin);

  if (sensorValue >= lickThresholdSup)
  {
    if (animalIsLicking == false)
    {
      digitalWrite(ledPin, HIGH);
      animalIsLicking = true;
      outputValue = lickThresholdSup;
    }
  }

  else
  {

    if (animalIsLicking == true)
    {
      if (sensorValue <= lickThresholdInf)
      {
        digitalWrite(ledPin, LOW);
        animalIsLicking = false;
        outputValue = lickThresholdInf;
      }
    }
  }

  // print the results to the Serial Monitor:
  Serial.print("sensor = ");
  Serial.print(sensorValue);
  Serial.print("\t output = ");
  Serial.println(outputValue);


  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  // delay(2);
}
